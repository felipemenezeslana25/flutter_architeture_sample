# Flutter Architeture Sample

## Features

São ações que o usuário irá executar durante o uso do aplicativo. 

Exemplos:
* Comprar créditos
* Escolher uma rota a ser traçada
* Listar alunos de uma sala
* Pesquisar por uma trilha 

### Presentation

Camada responsável por:
* Desenhar os **widgets** na tela
* Disparar **eventos** que trigam ações/rotinas dentro do aplicativo
* Escuta por **trocas de estado**

#### bloc

#### pages

#### widgets

### Domain (Lógica de negócio e Entidades)

Essa camada representa o que o aplicativo de fato executa para o usuário. 
É nela que se comunica com as API externas, e a lógica de negócio é executa. 

As Entidades representam objetos que serão manipulados durante a execução da lógica de negócio.

Deve-se garantir que essa camana não dependa de nenhuma outra no sistema.

Pense nessa camada como a descrição do que o aplicativo precisa fazer.

#### entities

#### repositories

#### usecases

### Data

Essa camada é a implementação concreta de onde os dados são adquiridos. 
Eles podem vir de um banco de dados local ou remoto, pode vir de arquivos de texto, binários e outros, além de APIs externas que podem ser consumidas pelo 

Pense nessa camada como a execução concreta da descrição do aplicativo.

É como se ela trabalhasse para atender a camada _domain_